Hi Nathan,

This is a truly excellent paper. Expanded to full article length, this could
easily be PhD-level work and could even be revised into something publishable.
The scale and scope of the project are ambitious without becoming blobby, your
evidence is broadly collected but well sifted, and you combine primary,
secondary, visual, and textual material quite seamlessly. I’m also convinced by
your argument. We should see Japanese mapping as assimilationist in multiple
registers at once—not just as a tool of economic control, not just as a form of
popular media (“propaganda”), but above all as a way of thoroughly replacing the
previous spatial categories that regulated Korea from the top and the bottom
alike. This isn’t about one or even several particular maps, but about an
infusion of a new spatial logic; maps were a part of this project but they’re
also evidence of its much more ambitious reach, beyond the visual to the
cultural and categorical. This is great stuff.

My main substantive comment is that the paper as a whole would be stronger if
you expanded your introduction and gave your ideas some room for development.
Everything you say in these two paragraphs is sharp and concise, but the opening
is a bit abrupt (some kind of analytic or historical hook could ease us into the
ideas more smoothly) and both your scholarly intervention and your argument are
bit telegraphic (they get us interested, but on its own they’re not enough to
really understand the stakes). Just allowing yourself to double or triple the
word count here would go a long way. Similarly, the conclusion could easily be
expanded as well (and the work of the conclusion foreshadowed in the intro),
perhaps with some broader thoughts about how you understand spatial rule, the
relationship between different sorts of maps (or between maps and other metrics
of surveillance and governance), and so forth. And the last sentence right now
is weak; instead of some vague reference to the present you’d either want to
develop this properly or keep your focus on the lessons here for the colonial
period.

My other comments are relatively minor. On page 9, I don’t think the Japanese
coordinate system was “international” at the time. I think it was pegged to the
observatory in Tokyo (which, IIRC, happens to be on a huge geoidal slope and so
really has problems if extended too far), and an “international” system wouldn’t
come until after World War II. But I might be wrong here! For figures 6 and 7
(and even 8), which are crucial for your argument, I think they’d be more
convincing if presented comparatively. For example, you could compare figure 6
with a pre-colonial map of Seoul. Figure 7 has obvious similarities to the
longstanding oblique-view tradition in Japan, and you could more directly
compare this map with Japanese examples (I’m attaching one I happen to have,
which unfortunately I know almost nothing about, but I’ve seen similar ones
elsewhere, perhaps most memorably in one of Edward Tufte’s books). For the
second part of the paper what you want to show is _change_ and _Japanization_,
not just maps in isolation. (You might also want to explain your use of
“propaganda,” or discard it altogether in favor of something more like
Japanization.)

But these are all just meant as suggestions for improvement, not criticisms of
what you’ve already managed to produce. This is a really impressive paper!

Paper Grade: A+

It was great to have you in the class this term. I know it was a topsy-turvy
time for all of us, but you always contributed to class with enthusiasm and
generosity. Your discussion questions were consistently excellent, and your
participation was great. I hope we get the chance to cross paths again—perhaps
even in person!

My best wishes for a restful January, Bill
