- extraneous information
  - kangnido
  - kim chon
  - japanese maps transitioning

Just give historical context instead of giving examples of maps (which take up
space and detract from the focus in the paper).

- Spend more time on the intro

- wording has too many details and not enough synthesis/story/argument
  - there's a bit of syntehsis in the paragraph satrting with "But the
    Bureau's..." that should be taken further here and brought up earlier in the
    paper
- just restating arguments and spending time coming up with one-liners could
  help this paper a lot. like so what does the metaphor of the body or the
  top-down imagery have to do with assimilation or consent? there's a very
  obvious story that just needs a few more sentences of elaboration, not even
  research
  - some more: in discussion of japanese maps, say something like "the Japanese
    cadastral project thus uprooted and replanted spatial knowledge in the soil
    of the Japanese empire" or something corny like that. Sell it
- Too many commas and incomplete first clauses
- assumes that the reader already knmows a bit about japanese rule (some
  background should have been given)

- Good section: The map thus served as abridge from material to cultural rule,
  translating Japanese physical developments into culturally recognizable
  images. This link between materialityand 19As a reference, Broadway above
  59thStreet in Manhattan is 45meters wide,and the average avenue in Manhattan
  is100feet wide (about 30 meters). Taihei Boulevard was really wide. discourse
  was precisely what was needed for the Empire after years of physical rule were
  unable to preventrebellion
