# Telling time with space: Colonial imagination of the Dakota Territory

Nathan Kim

September 30^th^, 2020

Maps: they tell us something about space. They also communicate ideas about
time, not only portraying the era in which they were made but offering specific
views on when places were named, who came before, and even what will happen next
in the region shown. For the colonial eye, this vision can act as a visual
argument for expansion and justifies both prior and future violence. We can see
this especially in Rice's sectional map of Dakota Territory of 1872, a U.S.
cartographer's imagination of Westward Expansion. This map was made during a
transition: 1872 is a few years after the completion of the Transcontinental
Railroad in 1869 but before the Dakota Territory is incorporated as North and
South Dakota in 1889.  It was also a transition from independent Indigenous
nations to a set of reservations under U.S. rule, through several wars beginning
with the First Sioux War in 1854 and culminating with the Ghost Dance War
in 1890. Most of the Sioux territory that had been protected after the Red
Cloud's War of 1866 by this point was broken up into the counties that exist in
the Dakotas today. Agreeing with all of these histories, Rice's map views the
territory as within a key transitional moment. The map's history also puts forth
a distinct vision for what the future should bring, foregrounding the next phase
of settler colonialism centered around the expanding Northern Pacific Railroad.
Rice's map ultimately puts forth a vision for future settlers that the land
should be pushed from its "natural" -- conflated with Indigeneity -- state into
a civilized order ruled only by the United States.

The "past" according to this map is of a natural world, conflated with
Indigeneity, that has already been conquered through physical and discursive
violence. Sometimes the "past" is shown explicitly with dates, seen next to
several battles and previous expeditions. Rice does not include every battle
since the beginning of the Sioux Wars, and his naming of battles are likely some
of the first maneuvers to create a "canon" of what constituted noteworthy
battles. Even the decision to mark major battles is itself questionable, given
that most offensive efforts by the Sioux during the Wars were raids performed in
small groups rather than battles a cartographer could easily point to on a map.
The two battles he does choose to include are both decisive American victories,
where the U.S. army not only defeated Sioux troops but also massacred Sioux
civilians and children and destroyed the winter supplies of jerky. As if to
stake the U.S.' claims on former Indigenous land, Rice marks these sites with a
small American flag. A related theme of near-completed conquest is seen with the
dated expeditions throughout the map, which together with the battles emphasize
that though the region is not yet fully settled, exploring and conquering this
territory has already been undertaken. The region is not the complete
wilderness, but instead a U.S.-managed area thanks to the work of those that
came before.

![](media/image1.png){width="6.0in" height="2.7084612860892388in"}

**Figure 1. Examples of dated surveys and expeditions (top four), examples of
dated battles (bottom two). These and all other figures come from Rice, G. Jay,
Fred Sturnegk, and St. Paul Litho. & Engr. Co. _Rice\'s sectional map of Dakota
Territory._ St. Paul, Minn.: St. Paul Lithog. & Eng. Co, 1872. Map.
https://www.loc.gov/item/2012593215/.**

For Rice's map, the past also means a juncture between multiple groups -- the
British, French, American, and the Sioux. Many French names can be seen
throughout the map, the largest being "Plateau du Couteau du Missouri" and also
including "l'isle de Bois Brute," "Maison du Chieu," "leau qiu monte." The
presence of these is especially notable given that France had last held the
region in 1803, and because there are only a few names borrowed from the Sioux
nations. The presence of the long-gone French is memorialized through these
names, while the names from those who still live in these areas at the time of
the map's creation are ignored. Still, the number of French names are small
compared to the number of names in English, and some French names in Rice's map
often have translations besides them in English. In other words, even as the map
remembers the French, it makes sure to do so on English terms. Finally, the
juxtaposition of British-held land at the top of the map gives the impression
that although Britain is close by, the territory shown in the map is definitely
_not_ British, again connoting that the United States has established at least a
formal grasp on the Territory. All of these details suggest that the United
States is at the top of an international hierarchy over sovereignty in the
Dakota Territory.

![](media/image2.png){width="5.971698381452319in" height="3.8229068241469815in"}

**Figure 2. Examples of British-French-Sioux-American Convergence in the Dakota
Territory.**

These images of the past create this map's view of the "present:" a territory
under transition to be placed under U.S. rule, the process already begun but not
yet completed. The tension between these states can be seen in the visual
hierarchy of lines in the map. The Dakota Territory shown here has few borders,
making it difficult to see where the Dakota Territory and other regions begin as
well as impossible to see where other regions stop and end. This is especially
so with the Nebraska-Dakota division in the bottom section of the map. The
division between the Dakota and Nebraska Territories was defined in 1861 from
west to east as the line marking 43° latitude, then the Keya Paha River after it
crosses below the 43° line, and finally the Niobrara River after it merges with
the Keya Paha River. However, the 43° latitude line is not easily
distinguishable from other longitude/latitude lines save for thin vertical
strokes slanting northeast to southwest. These vertical strokes disappear
entirely shortly after the division becomes the Keya Paha River, after which no
visual markers clearly denote the border. The labels for "Nebraska" are more
noticeable than the border itself, stretching across the map in bold,
capitalized letters, but even this marker meanders and curves instead of
following the 43° line. These words are also difficult to visually distinguish
with the title of the map in the bottom center. In other words, although there
is clearly a section of land that is "Nebraska" and a section that is "Dakota,"
these sections are not clearly defined through borders.

![](media/image3.png){width="5.5739129483814525in" height="1.810926290463692in"}

**Figure 3: The Nebraska-Dakota Divide**

Examining the place of borders within the visual hierarchy shows us how this map
situates the present as well. In increasing order of thickness, Rice also draws
wagon trails, routes of expeditions and surveys, railroad tracks, streams,
outlines of geological formations like the _Plateau du Couteau du Missouri,_ and
finally larger rivers like the Red River in the East and the Missouri River in
the center portion of the map. Railroad executives might be trained from
experience to look for the Northern Pacific Railway ("N.P.R.R.", drawn near the
47° line) first, but for new settlers the Missouri River or the Black Hills
would likely catch the eye first because of the detail and opacity Rice uses to
draw them.

To contradict this order are the array of gridded counties in the Southeast and
Pembina county in the Northeast. These grids put state divisions above rivers,
obscuring "nature" with ordered "civilization." The lines separating each county
are even thicker than the lines separating Dakota Territory from other regions,
and they consistently have lines slanting northeast to southwest while they are
used only inconsistently to show the separation between the Dakota territory
from neighboring regions. Hutchinson, Jayne, Buffalo, and Minnehaha counties are
incompletely gridded, suggesting that progression within these counties or from
these counties to land further north is yet to come. While the main visual
hierarchy of lines in this map puts "nature" above "man-made" artifacts like
borders and roads, the portrayal of counties also argues that civilization has
laid its first steps in the region, and more is yet to come. New settlers and
railroad workers are encouraged to join the settler colonial project and settle
what is otherwise an expanse of nature.

![](media/image4.png){width="5.056604330708661in" height="3.601211723534558in"}

**Figure 4: Gridded counties, the ungridded Yankton reservation.**

The map also situates the present through strategic management of Indigenous
territory. Like the map's use of the battles of the Sioux Wars, the portrayal of
Indigenous nations in the "present" of 1872 implies a victory for the United
States. The Yankton Reservation is shown next to Missouri River, surrounded by
gridded counties but conspicuously not gridded itself. This juxtaposition places
the Yankton nation in opposition to the rationally ordered set of U.S. counties.
If the counties are the "present" manifestation of an ordered future to come,
the Yankton reservation is shown as the enclosed "past" of the Yankton Sioux,
and ultimately conflated with nature because it lacks the system of grids shown
on the counties. The Sisseton and Warpeton reservation in the center-west of the
map gives a similar message with different methods. While there are no counties
in this region to imply that the Indigenous territory has been "enclosed," the
reservation itself is divided into a grid as well. The Sissteon and Warpeton
nations have been managed in the same empire that continues to stretch into the
Territory, no longer posing a threat.

![](media/image5.png){width="4.433962160979878in" height="6.724031058617673in"}

**Figure 5. The Sisseton and Warpeton reservation.**

What speaks loudest in this project of Indigenous nation management are what
territories are not noted at all. The First Sioux War in 1856, the Dakota War of
1862, the Powder River war of 1865, and the Red Cloud\'s War of 1866 had all
been fought at the time of this map\'s creation, to the effect of Indigenous
territory being defined through numerous treaties. The Red Cloud\'s War in
particular resulted in a treaty signed that allocated all land west of the
Missouri River in the Dakota Territory as the Great Sioux Reservation, a
designation which remained until 1887. This massive allocation is not shown in
the map here at all, and instead the Sioux territory is just another part of
nature not yet fully "settled." The region west of the Missouri River also lacks
the same level of detail as the region east of the River, suggesting that the
region is a section of nature that has yet to be fully explored rather than an
area curated by the Sioux nation. As opposed to another nation limiting Westward
expansion, the reader sees instead vast expanses of land where Americans have
not yet been.

In Rice's map, these histories of the "past" and "present" are oriented for a
specific vision of what the "future" should bring. Given the many
railroad-related elements seen here and the fact that the map was distributed in
Minnesota, a region that was already incorporated as a state in 1858, this map
likely would have been oriented towards future settlers and affiliated of
railroad companies. The size of the map at 27 by 34 inches is moderately large,
but was still a foldable pocket map on a cloth backing, suggesting that the map
was to be referred often by those currently stepping out onto the territory
instead of executives or politicians on an office wall. The legend of the map
supports this -- the only lines that Rice chooses to differentiate here are for
railroads, railroad explorations, wagon roads, trails, and boundary lines, which
are useful not exactly for office-based administrators but instead settlers and
railroad companies who are planning for travel or traveling through the
territory.

![](media/image6.png){width="5.528301618547681in" height="6.250281058617673in"}

**Figure 6. The map legend**

By catering to these readers in particular, the map advances a vision of this
space's future of capitalist and colonial settlement on an already-conquered
territory. This specific vision can be seen in the map's care to note unfinished
railroads and railroad explorations, which are not immediately useful as
concrete markers for settlers in 1872 but useful as suggestions for what next
year's Dakota territory might look like. Wagon roads and trails do related work
here of enticing people into the territory.

One more clue in this map's audience and purpose for future readers can be seen
in the detail given to elevation. Rice's map pays close attention to elevation
by either explicitly labeling elevation or showing many small marks to indicate
a change in elevation. These tick marks even appear more visible than county or
territory borders and are seen in every section of the map. While elevation is
not incredibly important for individual settlers moving with wagons, elevation
does affect where railroads can and cannot be built as well as where supplies
for railroads can be brought. Trains of this time period weighed several tons
and were difficult to brake or increase speed on command, making even moderately
tall hills obstacles to either build around or tunnel through with dynamite.
Rice's map caters both to settlers and railroad companies, beckoning settlers by
showing wagon trails and areas already under settlement and acting as a railroad
company's guide for where tracks may or may not be laid.

![](media/image7.png){width="6.037736220472441in" height="3.356233595800525in"}

**Figure 7. Elevation markers: Bad lands (3400ft), Sand Hills (2350 ft), Turtle
Hill (2340 ft)**

The history of the region somewhat contradicts Rice's map, but it is clear how
Rice's map foregrounds even these contradictions. The years following 1872 were
not a simple progression of victorious Americans onto vast expanses of
already-conquered land, as Rice's map suggests, but a continuation of the Sioux
Wars for nearly twenty years until the end of the Ghost Dance War of 1890
and 1891. Rice's map plays a sleight of hand in suggesting that the land was
already conquered for the overall goal of ensuring that these lands are brought
into U.S. control. While the map might have been considered outdated as early as
the next year (and a new edition was published in 1874), Rice's map tries to
stretch beyond and conjecture into these years anyhow. A single map doesn't
portray a single moment, but a historical continuation of what came before and
what might come after.
